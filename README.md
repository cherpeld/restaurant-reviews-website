# Lancez votre propre site d'avis de restaurants

## Etape 1 : Génération de la carte

Fondations de votre application. Il y aura 2 sections principales :
* Une carte Google Maps, chargée avec l'API de Google Maps
* Une liste de restaurants correspondant à la zone affichée sur la carte Google Maps

Ces restaurants sont affichés grâce à leurs coordonnées GPS sur la carte. Les restaurants qui sont actuellement visibles sur la carte doivent être affichés sous forme de liste sur le côté de la carte. La moyenne des commentaires de chaque restaurant est affichée (qui va de 1 à 5 étoiles).

Lorsqu'on clique sur un restaurant, la liste des avis enregistrés s'affiche avec les commentaires (Ainsi que la photo Google Street View grâce à l'API correspondante).

Un outil de filtre permet d'afficher uniquement les restaurants ayant entre X et Y étoiles. La mise à jour de la carte s'effectue en temps réel.

## Etape 2 : Ajouter des restaurants et des avis !

Vos visiteurs aimeraient eux aussi donner leur avis sur des restaurants :
* D'ajouter un avis sur un restaurant existant
* D'ajouter un restaurant, en cliquant sur un lieu spécifique de la carte

Une fois un avis ou un restaurant ajouté, il apparaît immédiatement sur la carte. Un nouveau marqueur apparaît pour indiquer la position du nouveau restaurant.

Les informations ne seront pas sauvegardées si on quitte la page (elles restent juste en mémoire le temps de la visite).

## Etape 3 : Intégration avec l'API de Google Places