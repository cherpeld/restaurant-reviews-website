/** @description Effet pour l'introduction
 *
 */
$("#presentation").fadeIn("slow");
function eraseIntroduction() {
    $("#presentation").fadeOut("slow");
}
setTimeout(eraseIntroduction, 1500);


// On initialise la latitude et la longitude de Paris (centre de la carte)
var lat = 48.852969;
var lon = 2.349903;
var map;


/** @description Au chargement de la page : choix de la géolocalisation ou non par l'utilisateur
 *
 */
window.onload = function() {
    map = new MyMap(lat, lon);
    position = new google.maps.LatLng(lat, lon);

    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;

            const request = {
                location: {lat, lng: lon},
                radius: '500',
                type: ['restaurant']
            };

            map.setUserMarker(lat, lon);
            map.getRestaurantList(request, map.map);
            //map.getLocalRestaurantList();
            map.createMarker();
            map.getRestaurantWhenDragend()

        }, function () {

            const request = {
                location: {lat, lng: lon},
                radius: '500',

                type: ['restaurant'],
            };

            alert('La position par défaut à été définie sur Paris.');
            map.setUserMarker(lat, lon);
            map.getRestaurantList(request, map.map);
            //map.getLocalRestaurantList();
            map.createMarker();
            map.getRestaurantWhenDragend()
        },{timeout:10000});
    }

};

/** @description Recharge la map
 *
 */
function resetMap() {
    map.cleanMarkers();
    window.onload();
}


/** @description Création element dans la barre gauche
 *
 * @param restaurant
 *
 */
function createItemsList(restaurant) {

    const btnElement = document.createElement('button');

    let restaurants = document.getElementById('list-restaurants');
    restaurants.appendChild(btnElement);
    btnElement.className = "btn-restaurant";

    let divImgRestaurantElement = document.createElement("div");
    divImgRestaurantElement.className = "restaurant-img-left";
    btnElement.appendChild(divImgRestaurantElement);

    let imgElement = document.createElement('img');
    imgElement.className = "img-btn-restaurant";
    divImgRestaurantElement.appendChild(imgElement);
    imgElement.src = "https://maps.googleapis.com/maps/api/streetview?size=312x240&location=" + restaurant.lat + "," + restaurant.long + "&heading=151.78&pitch=-0.76&key=AIzaSyAf5QPWBhOSiX8VJ1DN8MfBPNyskfL4m6A";

    const divInfoElem = document.createElement("div");
    divInfoElem.className = "info-restaurant";
    btnElement.appendChild(divInfoElem);
    const ulElement = document.createElement('ul');
    ulElement.className = 'restaurant';
    ulElement.dataset.id = restaurant.id;
    divInfoElem.appendChild(ulElement);


    btnElement.addEventListener("click", (e) => {
        let modal = document.getElementById("modal1");
        modal.style.display = 'flex';

        createElemModal(restaurant)
    });

    let liElement = document.createElement('li');
    liElement.innerText = restaurant.restaurantName;
    liElement.className = "restaurant-name-left";
    ulElement.appendChild(liElement);

    liElement = document.createElement('li');
    liElement.innerText = restaurant.address;
    liElement.className = "restaurant-adress-left";
    ulElement.appendChild(liElement);

    liElement = document.createElement('li');
    liElement.className = "restaurant-rating-left";
    liElement.innerText = 'Notes';
    ulElement.appendChild(liElement);

    let divElement = document.createElement('div');
    ulElement.appendChild(divElement);

    let resultStars = restaurant.getAverage();

    //Etoiles couleur
    for (var i = 0; resultStars > i; i++){

        let iElement = document.createElement('i');
        divElement.appendChild(iElement);
        iElement.className = 'fa fa-star yellow-star';
    }

    //Etoiles noirs
    for (var i = 0; 5 - resultStars > i ; i++) {
        let iElement = document.createElement('i');
        divElement.appendChild(iElement);
        iElement.className = 'fa fa-star';
    }
}

for (let i = 0; i < listRestaurantClass.listRestaurant.length ; i++) {
    const restaurant = listRestaurantClass.listRestaurant[i];
    createItemsList(restaurant);
}


let filtered = document.getElementById('filtered');

/** @description Filtre des notes des restaurants
 *
 *
 */
filtered.addEventListener("change", function (event) {
    listRestaurantClass.filterRestaurant(event.target.value);
    document.getElementById("list-restaurants").innerHTML = "";
    for (var i = 0; listRestaurantClass.listRestaurant.length > i;i++){
        const restaurant = listRestaurantClass.listRestaurant[i];
        createItemsList(restaurant)
    }
});

let filteredHeader = document.getElementById('filtered-header')

filteredHeader.addEventListener("change", function (event) {
    listRestaurantClass.filterRestaurant(event.target.value);
    document.getElementById("list-restaurants").innerHTML = "";
    for (var i = 0; listRestaurantClass.listRestaurant.length > i;i++){
        const restaurant = listRestaurantClass.listRestaurant[i];
        createItemsList(restaurant)
    }
});


/** @description Création element dans la modale restaurant
 *
 * @param restaurant
 *
 */
function createElemModal(restaurant){

    const areaModal = document.getElementById("restaurant-modal");
    areaModal.innerText = "";

    const divHeaderModal = document.createElement('div');
    divHeaderModal.className = "header-modal";
    areaModal.appendChild(divHeaderModal);

    const divImgElement = document.createElement("div");
    divImgElement.className = "modal-image";
    divHeaderModal.appendChild(divImgElement);

    let imgElement = document.createElement('img');
    imgElement.className = "restaurant-img-modal";
    divImgElement.appendChild(imgElement);
    for (i = 0; listRestaurantClass.listRestaurant.length > i; i++){
        imgElement.src = "https://maps.googleapis.com/maps/api/streetview?size=312x240&location=" + restaurant.lat + "," + restaurant.long + "&heading=151.78&pitch=-0.76&key=AIzaSyAf5QPWBhOSiX8VJ1DN8MfBPNyskfL4m6A";
    }

    const divUlElement = document.createElement("div");
    divUlElement.className = "modal-ul";

    divHeaderModal.appendChild(divUlElement);
    const ulElement = document.createElement('ul');
    divHeaderModal.appendChild(ulElement);
    ulElement.className = "restaurant";
    ulElement.id = "information";
    ulElement.dataset.id = restaurant.id;

    divUlElement.appendChild(ulElement);

    let liElement = document.createElement('li');
    liElement.innerText = restaurant.restaurantName;
    liElement.className = "restaurant-name-modal";
    ulElement.appendChild(liElement);

    liElement = document.createElement('li');
    liElement.innerText = restaurant.address;
    liElement.className = "adress-restaurant-modal";
    ulElement.appendChild(liElement);

    liElement = document.createElement('li');
    liElement.innerText = 'Notes';
    liElement.className = 'ratings-restaurant-modal';
    ulElement.appendChild(liElement);

    let divElement = document.createElement('div');
    divElement.className = "stars-restaurant-modal";
    ulElement.appendChild(divElement);

    let resultStars = restaurant.getAverage();

    //Etoiles couleur
    for (var i = 0; resultStars > i; i++){

        let iElement = document.createElement('i');
        divElement.appendChild(iElement);
        iElement.className = 'fa fa-star yellow-star';

    }

    //Etoiles noirs
    for (var i = 0; 5 - resultStars > i ; i++) {
        let iElement = document.createElement('i');
        divElement.appendChild(iElement);
        iElement.className = 'fa fa-star';
    }

    //Espace avis
    const divBodyModal = document.createElement("div");
    divBodyModal.className = "body-modal";
    areaModal.appendChild(divBodyModal);
    let spanElement = document.createElement('span');
    spanElement.innerText = 'Avis :';
    spanElement.className = "view-modal";
    divBodyModal.appendChild(spanElement);

    let comment = document.createElement('span');
    comment.className = 'comment';
    divBodyModal.appendChild(comment);

    for (var i = 0; restaurant.rating.length > i; i++) {
        let divElem = document.createElement("div");
        divElem.className = "col-sm-9";
        for (var j = 0; restaurant.rating[i].stars  > j ; j++) {
            let iElement = document.createElement('i');
            divElem.appendChild(iElement);
            iElement.className = 'fa fa-star star-new-restaurant star-default';
        }

        //Etoiles noirs
        for (var j = 0; 5 - restaurant.rating[i].stars > j ; j++) {
            let iElement = document.createElement('i');
            divElem.appendChild(iElement);
            iElement.className = 'fa fa-star star-new-restaurant';
        }
        let pElement = document.createElement("p");
        pElement.className = "comment-text";
        pElement.innerText = restaurant.rating[i].comment;
        comment.appendChild(divElem);
        comment.appendChild(pElement)
    }

    let newComment = document.createElement('span');
    comment.id = 'new-comment';
    divBodyModal.appendChild(newComment);
}

/** @description Ferme la modale restaurant
 *
 */
function closeModal() {
    document.getElementById("modal1").style.display = 'none';
}

//Ouvrir/Fermer formulaire nouveau commentaire
let btnAddcomment = document.getElementById('add-comment');
let starsAddComment = document.getElementById('stars-add-comment');
let formAddComment = document.getElementById('comment-form');

/** @description Evenement sur le click des étoiles
 *
 */
starsAddComment.addEventListener("click", function (event) {
    selectStars('stars-add-comment', event.srcElement.dataset.value)
});

/** @description Ouvre/ferme l'espace nouveau commentaire
 *
 */
function OpenCloseAddComment(){
    if(getComputedStyle(formAddComment).display != "none"){
        formAddComment.style.display = "none";
    } else {
        selectStars("stars-add-comment", 1);
        formAddComment.style.display = "block";
    }
};

btnAddcomment.onclick = OpenCloseAddComment;


/** @description Envoie un nouveau commentaire
 *
 */
function sendComment() {
    let restaurantId = document.getElementById("information");
    let newComment = document.getElementById('comment');
    let stars = document.getElementById('stars-add-comment');

    for (var i = 0; listRestaurantClass.listRestaurant.length >i; i++) {
        if (listRestaurantClass.listRestaurant[i].id === parseInt(restaurantId.dataset.id)) {
            listRestaurantClass.listRestaurant[i].rating.push({"comment": newComment.value, "stars": parseInt(stars.dataset.value)});
            let areanewComment = document.getElementById('new-comment');

            areanewComment.innerHTML = "";
            for (var j = 0; listRestaurantClass.listRestaurant[i].rating.length > j; j++ ){
                areanewComment.innerHTML += "<p>" + listRestaurantClass.listRestaurant[i].rating[j].comment + "</p><br>";
            }
        }
    }
    listRestaurantClass.filterRestaurant("");
    OpenCloseAddComment();
    newComment.value = "";

    document.getElementById("list-restaurants").innerHTML = "";
    for (var i = 0; listRestaurantClass.listRestaurant.length > i;i++){
        const restaurant = listRestaurantClass.listRestaurant[i];
        createItemsList(restaurant);

        if (restaurant.id === parseInt(restaurantId.dataset.id)){
            createElemModal(restaurant)
        }
    }

}

/** @description Création des étoiles de la modal du nouveau restaurant
 *
 *  @param idStars
 *  @param numStars
 *
 */
function selectStars(idStars, numStars) {
    const areaStars = document.getElementById(idStars);
    areaStars.innerHTML = "";
    let istars = 1;

    areaStars.dataset.value = numStars;

    //Etoiles couleur
    for (var i = 0; numStars  > i ; i++) {
        let iElement = document.createElement('i');
        areaStars.appendChild(iElement);
        iElement.className = 'fa fa-star star-new-restaurant star-default';

        iElement.dataset.value = istars;
        istars++;
    }

    //Etoiles noirs
    for (var i = 0; 5 - numStars > i ; i++) {
        let iElement = document.createElement('i');
        areaStars.appendChild(iElement);
        iElement.className = 'fa fa-star star-new-restaurant';

        iElement.dataset.value = istars;
        istars++;
    }
}

/** @description Envoie le nouveau restaurant au tableau listRestaurant
 *
 */
function createNewRestaurant() {

    const newRestaurant = document.getElementById("modal2");
    let newNameRestaurant = document.getElementById("new-name-restaurant");
    let newAddressRestaurant = document.getElementById("new-address-restaurant");
    let newCommentRestaurant = document.getElementById("new-comment-restaurant");
    const areaStars = document.getElementById("stars-new-restaurant");

    const newClassRestaurant = new Restaurant(null, newNameRestaurant.value, newAddressRestaurant.value, newRestaurant.dataset.lat, newRestaurant.dataset.lng, [
        {
            "stars": areaStars.dataset.value,
            "comment": newCommentRestaurant.value
        },
    ]);
    listRestaurantClass.addRestaurant(newClassRestaurant);
    newNameRestaurant.value = "";
    newAddressRestaurant.value = "";
    newCommentRestaurant.value = "";
    listRestaurantClass.filterRestaurant("");
    document.getElementById("list-restaurants").innerHTML = "";
    for (var i = 0; listRestaurantClass.listRestaurant.length > i;i++){
        const restaurant = listRestaurantClass.listRestaurant[i];
        createItemsList(restaurant)
    }
    closeModalNewRestaurant();
    map.placeMarker({lat: parseFloat(newRestaurant.dataset.lat), lng: parseFloat(newRestaurant.dataset.lng)}, map);

}

/** @description Ferme la modale du nouveau restaurant
 *
 */
function closeModalNewRestaurant() {
    const newRestaurantModal = document.getElementById("modal2");
    newRestaurantModal.style.display = "none";
}

let btnAddNewRestaurant = document.getElementById("add-new-restaurant");
btnAddNewRestaurant.onclick = createNewRestaurant;
