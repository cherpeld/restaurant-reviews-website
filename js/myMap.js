/** Class representant la map */
class MyMap {
    /**
     *
     * Création de la map
     *
     * @param latitude
     * @param longitude
     * _initMap(latitude, longitude)
     * markerList - tableau de tous les markers
     *
     */
    constructor(latitude, longitude) {
        this._initMap(latitude, longitude);
        this.markerList = [];
    }

    /**
     **_initMap
     *
     * Methode privée pour les paramètres de la map
     *
     */
    _initMap() {
        this.map = new google.maps.Map(document.getElementById("map"), {
            center: new google.maps.LatLng(lat, lon),
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: true,
            scrollwheel: false,
            navigationControl: true,
            styles : [
                {
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        }
                    ]
                },
                {
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                },
                {
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#f5f5f5"
                        }
                    ]
                },
                {
                    "featureType": "administrative.land_parcel",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#bdbdbd"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#eeeeee"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e5e5e5"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#757575"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#dadada"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#616161"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                },
                {
                    "featureType": "transit.line",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e5e5e5"
                        }
                    ]
                },
                {
                    "featureType": "transit.station",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#eeeeee"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#c9c9c9"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#9e9e9e"
                        }
                    ]
                }
            ]
        });

    }

    /**
     **getRestaurantList
     *
     * Récupération des restaurants via GooglePlaces
     *
     * @param request
     * @param mapGoogle
     *
     */
    getRestaurantList(request, mapGoogle) {
        const service = new google.maps.places.PlacesService(mapGoogle);
        service.nearbySearch(request, (results, status) => {
            if (status === google.maps.places.PlacesServiceStatus.OK) {
                let limit = 9;
                results.forEach((restaurant) => {

                    //limite restaurants à 9 dans la liste
                    if (limit != 0){
                        const newRestaurant = new Restaurant(
                            null,
                            restaurant.name,
                            restaurant.vicinity,
                            restaurant.geometry.location.lat(),
                            restaurant.geometry.location.lng(),
                            [],
                            restaurant.place_id,
                        );
                        listRestaurantClass.addRestaurant(newRestaurant);
                        this.getGooglePlacesReviews(newRestaurant)
                        limit--;
                    }
                });
                map.createMarker();
                for (let i = 0; i < listRestaurantClass.listRestaurant.length ; i++) {
                    const restaurant = listRestaurantClass.listRestaurant[i];
                    createItemsList(restaurant);
                }
            }
        });
    }

    /**
     **getGooglePlacesReviews
     *
     * Requete pour recuperer les reviews
     *
     * @param restaurant
     *
     */
    getGooglePlacesReviews(restaurant) {
        const request = {
            placeId: restaurant.place_id,
            fields: ['review']
        };

        const service = new google.maps.places.PlacesService(this.map);
        service.getDetails(request, this.detailSearchCallBack(restaurant));
    }

    /**
     **detailSearchCallBack
     *
     * Récupération des détails restaurants (reviews)
     *
     * @param restaurant
     *
     */
    detailSearchCallBack (restaurant) {

        return function callbackPlaceDetails(result, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {

                var reviews = result.reviews || [];

                reviews.forEach(function (review) {
                    var resultDetailRating = review.rating;
                    var resultDetailComment = review.text;

                    restaurant.rating = {
                        "stars": resultDetailRating,
                        "comment": resultDetailComment,
                    };
                });
                listRestaurantClass.filterRestaurant("");
                document.getElementById("list-restaurants").innerHTML = "";
                for (var i = 0; listRestaurantClass.listRestaurant.length > i;i++){
                    const restaurant = listRestaurantClass.listRestaurant[i];
                    createItemsList(restaurant)
                }
            }
        }
    }

    /**
     **getLocalRestaurantList
     *
     * Récupération des restaurants via un fichier JSON en local
     *
     */
    getLocalRestaurantList() {
        fetch('./data.json')
            .then(response => response.json())
            .then(function(results) {
                results.forEach((restaurant) => {

                    const newRestaurant = new Restaurant(
                        null,
                        restaurant.restaurantName,
                        restaurant.address,
                        restaurant.lat,
                        restaurant.long,
                        restaurant.ratings
                    );
                    listRestaurantClass.addRestaurant(newRestaurant);
                });

                listRestaurantClass.filterRestaurant("");
                document.getElementById("list-restaurants").innerHTML = "";
                for (var i = 0; listRestaurantClass.listRestaurant.length > i;i++){
                    const restaurant = listRestaurantClass.listRestaurant[i];
                    createItemsList(restaurant)
                }

                this.map.addListener('rightclick', function(e) {
                    map.openModalNewRestaurant(e.latLng.lat(), e.latLng.lng())
                });
            });
    }

    /**
     **getCenter
     *
     *Renvoie la position affichée au centre de la carte
     *
     */
    getCenter(){
        return this.map.getCenter()
    }

    /**
     **getRestaurantWhenDragend
     *
     *Récupération des restaurants alentours après un déplacement sur la map
     *
     */
    getRestaurantWhenDragend() {
        this.map.addListener('dragend', () => {
            this.cleanMarkers();
            position = this.getCenter();
            const request = {
                location: {lat: position.lat(), lng: position.lng()},
                radius: '500',
                type: ['restaurant']
            };
            this.getRestaurantList(request, this.map);
        });
    }

    /**
     **cleanMarkers
     *
     * Methode pour effacer les markers des restaurants (et la liste des restaurants)
     *
     */
    cleanMarkers(){
        listRestaurantClass.cleanRestaurants();
        document.getElementById("list-restaurants").innerHTML = "";
        for (var i = 0; listRestaurantClass.listRestaurant.length > i;i++){
            const restaurant = listRestaurantClass.listRestaurant[i];
            createItemsList(restaurant)
        }
        for (let i = 0; i < this.markerList.length; i++) {
            this.markerList[i].setMap(null);
        }
    }

    /**
     **setUserMarker
     *
     * Methode pour le marker de l'utilisateur
     *
     * @param latitude
     * @param longitude
     *
     */
    setUserMarker(latitude, longitude) {
        let icon = {url: "./images/location.png",scaledSize: new google.maps.Size(40, 40), origin: new google.maps.Point(0,0), anchor: new google.maps.Point(25, 50)};
        let pos = {
            lat: latitude,
            lng: longitude
        };

        this.map.setCenter(pos);
        let marker = new google.maps.Marker({
            position: pos,
            map: this.map,
            title: 'Vous êtes ici',
            icon: icon
        });
        this.markerList.push(marker);
    }


    /**
     **createMarker
     *
     * Methode pour les markers des restaurants et evenement sur le click droit pour ajouter un nouveau restaurant
     *
     */
    createMarker(){

        let icon = {url: "./images/marker-food.png",scaledSize: new google.maps.Size(30, 30), origin: new google.maps.Point(0,0), anchor: new google.maps.Point(25, 50)};

        for (let i = 0; listRestaurantClass.listRestaurant.length > i; i++){
            let marker = new google.maps.Marker({
                position: {lat: listRestaurantClass.listRestaurant[i].lat, lng: listRestaurantClass.listRestaurant[i].long},
                title: listRestaurantClass.listRestaurant[i].restaurantName,
                draggable: true,
                icon: icon,
                map: this.map,
                customInfo: listRestaurantClass.listRestaurant[i].id
            });

            this.markerList.push(marker);
            marker.addListener('click', function(){
                let modal = document.getElementById("modal1");
                modal.style.display = 'flex';
                const restaurant = listRestaurantClass.listRestaurant.find(element => element.id == this.customInfo);
                createElemModal(restaurant)
            });

        }

        //Evenement sur le click droit
        this.map.addListener('rightclick', function(e) {
            map.openModalNewRestaurant(e.latLng.lat(), e.latLng.lng())
        });

    }

    /**
     **placeMarker
     *
     * Methode pour le nouveau marker sur la map
     *
     */
    placeMarker(position) {
        let icon = {url: "./images/new-marker-food.png",scaledSize: new google.maps.Size(30, 30), origin: new google.maps.Point(0,0), anchor: new google.maps.Point(25, 50)};

        let marker = new google.maps.Marker({
            position: position,
            map: this.map,
            icon: icon,
        });
        this.markerList.push(marker);
        this.map.panTo(position);
    }

    /**
     **openModalNewRestaurant
     *
     * Methode pour ouvrir la modale pour créer le nouveau restaurant
     *
     * @param lat
     * @param lng
     *
     */
    openModalNewRestaurant(lat, lng) {

        const newRestaurant = document.getElementById("modal2");
        newRestaurant.style.display = "flex";
        newRestaurant.dataset.lat = lat;
        newRestaurant.dataset.lng = lng;
        selectStars("stars-new-restaurant", 1);

        const areaStars = document.getElementById("stars-new-restaurant");

        //Evenement sur le click étoiles
        areaStars.addEventListener("click", function (event) {
            selectStars("stars-new-restaurant", event.srcElement.dataset.value);
        })
    }

}