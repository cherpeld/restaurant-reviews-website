/** Class representant un restaurant */
class Restaurant{
    /**
     *
     * Création d'un restaurant
     *
     * @param {number} restaurantId
     * @param {string} restaurantName
     * @param {string} address
     * @param lat
     * @param long
     * @param rating
     * @param placeId
     *
     */
    constructor(restaurantId, restaurantName, address, lat, long, rating, placeId = null){
        this._restaurantId = restaurantId;
        this._restaurantName = restaurantName;
        this._address = address;
        this._lat = lat;
        this._long = long;
        this._rating = rating;
        this._place_id = placeId;
    }

    /**
     **id
     *
     * Methode pour recuperer l'id du restaurant
     *
     */
    get id(){
        return this._restaurantId
    }

    /**
     **id
     *
     * Methode pour modifier l'Id du restaurant
     *
     */
    set id(newId){
        this._restaurantId = newId
    }

    /**
     **restaurantName
     *
     * Methode pour recuperer le nom du restaurant
     *
     */
    get restaurantName(){
        return this._restaurantName
    }

    /**
     **restaurantName
     *
     * @param newRestaurantName
     *
     * Methode pour modifier le nom du restaurant
     *
     */
    set restaurantName(newRestaurantName){
        this._restaurantName = newRestaurantName
    }

    /**
     **address
     *
     * Methode pour recuperer l'adresse du restaurant
     *
     */
    get address(){
        return this._address
    }

    /**
     **address
     *
     * @param newRestaurantAddress
     *
     * Methode pour modifier l'adresse du restaurant
     *
     */
    set address(newRestaurantAddress){
        this._address = newRestaurantAddress
    }

    /**
     **lat
     *
     * Methode pour recuperer l'adresse du restaurant
     *
     */
    get lat(){
        return this._lat
    }

    /**
     **lat
     *
     * @param newLat
     *
     * Methode pour modifier la latitude du restaurant
     *
     */
    set lat(newLat){
        this._lat = newLat
    }

    /**
     **long
     *
     *
     * Methode pour recuperer la longitude du restaurant
     *
     */
    get long(){
        return this._long
    }

    /**
     **long
     *
     * @param newLong
     *
     * Methode pour modifier la longitude du restaurant
     *
     */
    set long(newLong){
        this._long = newLong
    }

    /**
     **rating
     *
     * Methode pour recuperer les notes du restaurant
     *
     */
    get rating(){
        return this._rating
    }

    /**
     **rating
     *
     * @param newRating
     *
     * Methode pour modifier les notes du restaurant
     *
     */
    set rating(newRating){
        this._rating.push(newRating)
    }

    /**
     **place_id
     *
     * Methode pour recuperer place_id
     *
     */
    get place_id(){
        return this._place_id
    }

    /**
     **place_id
     *
     * @param newPlaceId
     *
     * Methode pour modifier place_id
     *
     */set place_id(newPlaceId){
        this._place_id = newPlaceId
    }


    /**
     **getAverage
     *
     * Methode pour calculer la moyenne des notes des restaurants
     *
     */
    getAverage() {
        let sum = 0;

        for (let i = 0; i < this._rating.length; i++) {
            sum = sum + this._rating[i].stars
        }

        return Math.round(sum/this._rating.length)
    }

}